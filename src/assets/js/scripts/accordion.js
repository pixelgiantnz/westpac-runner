require(['jquery'], function($) {
  'use strict';

  var $element = $('[data-accordian]');
  if (!$element.length) {return;}

  //console.log("data-accordian");

  // .udf.accordion = {

  // };$
  var $accordianItems = $(".c-accordian__item");
  var $accordianLinks = $(".c-accordian__link");
  var $accordianContent;

  $accordianLinks.on("click", function(){
    var $parent = $(this).parent();
    if(!$parent.hasClass("is-open")){
        $accordianItems.removeClass("is-open");
        $parent.addClass("is-open");
    } else {
        $parent.removeClass("is-open");
    }

  });

});
