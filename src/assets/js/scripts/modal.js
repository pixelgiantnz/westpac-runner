require(['jquery'], function($) {
  
  var $element = $('[data-featherlight]');
  
  if ($element.length) {
  	require(['featherlight'], function() {
	 		console.log("featherlight modal");
		  
		  var $gallery = $('[data-featherlight-gallery]');
		  if ($gallery.length) {

		  	require(['featherlight', 'featherlight-gallery'], function() {
		  		console.log("featherlight gallery");
		  		$gallery.featherlightGallery();
		  	});
		  }
	  });
  }

});
