(function($) {
  $(function() {
  'use strict';
    var $element = $('[data-matchheight]');
    if (!$element.length) {
      return;
    }
    //console.log("align height");

    require(["debounced", "matchHeight"], function() {
      var $wrapper = $element;
      var $listing = $(".w-col, .c-tile");
      $.fn.matchHeight._throttle = 600;

      $listing.matchHeight();

      $(window).smartresize(function(){
        if($(window).width() > 767 ){
          //resizeListingImage();
          $listing.matchHeight();
        } else {
          $listing.removeAttr("style");
        }
      });
    });
  });
})(jQuery);
