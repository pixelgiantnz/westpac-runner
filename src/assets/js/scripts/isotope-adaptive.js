(function($) {
  $(function() {
    var $element = $("#masonry-adaptive");
    if (!$element.length) {return false;}
    
    console.log("masonry grid");
    
    requirejs( [
      'assets/js/libs/isotope.pkgd.min.js',
      'assets/js/libs/debounced.js'
    ], function( Isotope ) {

      var $container = $element.find(".m-masonry__content"),
        $tagsContainer = $('#filters-tags'),
        $tags = $tagsContainer.find('.c-tag'), 
        $body = $('body'),
        windowWidth = null,
        colWlg = 340,
        colWsm = 200,
        columns = null;

      // $container.isotope({
      // // disable window resizing
      //   resizable: false,
      //   masonry: {
      //     columnWidth: colW
      //   }
      // });

      var iso = new Isotope( "#masonry-adaptive", {
        itemSelector: '.c-masonry-tile',
        resizable: true,
        masonry: {
          columnWidth: 1
        }       
      });

      function relayoutCentered(windowWidth, colWidth){
        var currentColumns = Math.floor( ( $body.width() - 10 ) / colWidth );
        if ( currentColumns !== columns ) {
          // set new column count
          columns = currentColumns;
          // apply width to container manually, then trigger relayout
          $container.width( columns * colWidth );
          //iso.layout();
            //.isotope('reLayout');
        }

        // if(windowWidth < 1425 && windowWidth > 1070){
        //   console.log(windowWidth < 1425 && windowWidth > 1070);
        //   var iso = new Isotope( "#masonry-adaptive", {
        //     itemSelector: '.c-masonry-tile',
        //     resizable: false,
        //     masonry: {
        //       columnWidth: 1
        //     }       
        //   });
        // }
        //iso.isotope( 'reLayout', reLayoutCallback );
        // var iso = new Isotope( "#masonry-adaptive", {
        //   itemSelector: '.c-masonry-tile',
        //   resizable: false,
        //   masonry: {
        //     columnWidth: colWidth
        //   }       
        // });
      }

      function reLayoutCallback(){
        console.log("reLayoutCallback");
      }

      $(window).smartresize(function () {
          windowWidth = $(window).width();
          //if(windowWidth > 1424){
            //console.log("windowWidth > 1424");
            //update columnWidth to a percentage of container width
            //relayoutCentered(colWsm);
            
          if(windowWidth < 1425 && windowWidth > 1070){
            //console.log("windowWidth < 1425 && windowWidth > 1070");
            relayoutCentered(windowWidth, colWsm);
          } else if(windowWidth > 1425){
            relayoutCentered(windowWidth, colWlg);
          }
          

      }).smartresize(); // trigger resize to set container width

    });
   

  });

})(jQuery);






// require(['jquery'], function ($) {

//   function IsotopeGrid(tilesId) {
//     console.log("IsotopeGrid");
//     this.$isotope = $(tilesId);
//     if (!this.$isotope.length) {
//       return;
//     }
//     console.log("data-tile-grid");

//     this.$isotopeItems  = this.$isotope.find('.b-tile');
//     this.options        = {
//       //defaultPageTitle      : this.$title.data('default-title'),
//       tagImageClasses       : 'b-tile',
//       //tagImageFilterClass   : 'image-filter',
//       stopResizeWindowWidth : 768,
//       columnWidth: 300
//     };

//   }

//   IsotopeGrid.prototype.init = function() {
//     console.log("isotope init");
//     (function(_this) {
//       requirejs(['isotope', 'debounced', 'imagesloaded'], function(Isotope) {
//         console.log("isotope require");
//         require(['jquery-bridget/jquery.bridget', 'packery'], function() {
//           console.log("bridget require");
//           $.bridget( 'isotope', Isotope );

//           _this.Layout       = new IsotopeGrid_layout(_this.$isotope, _this.options);

//          // _this.bindEvents();

//         });
//       });
//     })(this);
//   }

//   function IsotopeGrid_layout($isotope, options) {
//     console.log("layout");
//     this.$isotope = $isotope;
//     this.options  = options;

//     this.selectedTagFilter = '';

//     this.$tiles   = this.$isotope.find('.b-tile');

//     this.init();
//   }

//   IsotopeGrid_layout.prototype.init = function() {
//     console.log("layout prototype");
//     (function(_this) {
//       _this.$isotope.isotope({
//         itemSelector: '.b-tile',
//         // percentPosition: true,

//         layoutMode: 'packery',
//         masonry: {
//           columnWidth: _this.$tiles[0]
//         },
//         hiddenStyle: {
//           opacity: 0
//         },
//         visibleStyle: {
//           opacity: 1
//         },
//         transitionDuration: 0
//       });

//     })(this);
//   }

//   var tiles = new IsotopeGrid("#masonry-grid");


// });