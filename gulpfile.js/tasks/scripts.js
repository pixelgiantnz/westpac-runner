//var jsAll = "./src/assets/js/*.*";
// var jslibs ="./src/assets/js/libs/**/*.*";
//var jsScriptsAll = "./src/assets/js/scripts/*.js";
// var jsScriptsPath = "./src/assets/js/scripts";
// var jsVendorPath= "./src/assets/js/vendor";
// var jsVendorAll = "./src/assets/js/vendor/**/*.js";
// var jsBuildAll = "./build/assets/js/*.js";
//var jsBuildPath = "./build/assets/js";
//var jsBuildLibs = "./build/assets/js/libs";

var gulp         = require('gulp');
var browserSync  = require('browser-sync');
var gulpLoadPlugins = require('gulp-load-plugins');
var g               = gulpLoadPlugins();
var config       = require('../config/scripts');
var concat = require('gulp-concat');
var bowerFiles      = require('main-bower-files');

var swallowErr = function(err) {
  g.util.beep();
  console.log(err);
};

gulp.task('scripts', function() {
    gulp.src(config.src)
        .pipe(concat('main.js'))
        .pipe(gulp.dest(config.dest));
    gulp.src(config.scriptsSrc)
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(config.dest));
    gulp.src(bowerFiles())
        .pipe(g.filter('*.js'))
        .pipe(gulp.dest(config.libsDest))
        .pipe(browserSync.reload({stream:true}));
    gulp.src(config.libsSrc)
        .pipe(gulp.dest(config.libsDest));
    gulp.src(config.dest)
        .pipe(g.plumber({
          errorHandler: swallowErr
        }))
        .pipe(g.uglify())
        .pipe(g.rename({suffix: '.min'}))
        .pipe(gulp.dest(config.dest));
});

// gulp.task('scripts', function() {
//   // copy all the main scripts
//   gulp.src(jsAll)
//     .pipe(g.concat('main.js'))
//     .pipe(g.jshint().on('error', swallowErr))
//     .pipe(g.jshint.reporter('default'))
//     .pipe(gulp.dest(jsBuildPath))
//     .pipe(browserSync.reload({stream:true, once: true}));

//   // copy components
//   gulp.src(config.paths.src.javascript.scripts.all)
//     .pipe(g.concat('scripts.js'))
//     .pipe(g.jshint().on('error', swallowErr))
//     .pipe(g.jshint.reporter('default'))
//     .pipe(gulp.dest(jsBuildPath))
//     .pipe(browserSync.reload({stream:true, once: true}));

//   // // copy bower libraries files
//   gulp.src(bowerFiles())
//     .pipe(g.filter('*.js'))
//     .pipe(g.jshint().on('error', swallowErr))
//     .pipe(gulp.dest(jsBuildLibs))
//     .pipe(browserSync.reload({stream:true, once: true}));

//   // // copy custom libraries
//   gulp.src([jslibs])
//     .pipe(gulp.dest(jsBuildLibs));
// });

// gulp.task('minify:scripts', function() {
//   gulp.src(jsBuildPath)
//     .pipe(g.plumber({
//       errorHandler: swallowErr
//     }))
//     .pipe(g.uglify())
//     .pipe(g.rename({suffix: '.min'}))
//     .pipe(gulp.dest(jsBuildPath));
// });