var config = require('./')

module.exports = {
  src       : config.sourceAssets + '/sprites/*.svg',
  dest      : config.publicAssets + '/images/sprites',
  transform : {
    before : {
      parserOptions : { xmlMode: true }
    }
  }
}
