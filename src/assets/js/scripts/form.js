require(['jquery', 'validate'], function($) {
  'use strict';

  // $.udf.form = {
  //   $form: null,

  //   classes: {
  //     error    : "c-form__field-invalid",
  //     valid    : "c-form__field-valid",
  //     errorMsg : "c-form__error"
  //   },

  //   groups: {},

  //   loadGroups: function() {
  //     var $this = this;
  //     this.$form.find('input,textarea,select').filter('[data-form-group]').each(function(){
  //       var $element = $(this);
  //       if (!$this.groups[$element.data('form-group')]) {
  //         $this.groups[$element.data('form-group')] = "";
  //       }
  //       $this.groups[$element.data('form-group')] += $element.prop('name') + " ";
  //     });
  //   },

  //   validate: function() {
  //     var $this = this;
  //     console.log(this.groups);
  //     this.$form.validate({
  //       // debug: true,
  //       errorClass     : this.classes.error,
  //       validClass     : this.classes.valid,
  //       errorElement   : "em",
  //       groups         : $this.groups,
  //       errorPlacement : function(error, element) {
  //         // we assume all the select tags should have the wrapper
  //         error.addClass($this.classes.errorMsg);
  //         var wrap  = element.parent(".c-select");
  //         var group = element.parents(".c-form__element-group");
  //         if (group.length) {
  //           error.insertAfter(group);
  //         } else if (wrap.length) {
  //           error.insertAfter(wrap);
  //         } else {
  //           error.insertAfter(element);
  //         }
  //       }
  //     });
  //   },
  //   init: function(form) {
  //     this.$form = $(form);
  //     if (this.$form.data('form-validate')) {
  //       this.loadGroups();
  //       this.validate();
  //     }
  //   }
  // };
  // $(function() {
  //   $('[data-form]').each(function() {
  //     $.udf.form.init(this);
  //   });
  // });
});

