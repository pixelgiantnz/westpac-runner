(function($) {
  $(function() {
      $.udf = {
        elements: {
          body:   $('body'),
          window: $(window),
          html:   $('html'),
          wrap:   $('#l-wrap')
        },
        events: {
          click: "click touchstart"
        },
        supports: {
          geolocation: navigator.geolocation
        }
      };
    });
})(jQuery);