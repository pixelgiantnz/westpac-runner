var gulp    = require('gulp');
var config  = require('../config/deploy');
var rsync    = require('gulp-rsync');

gulp.task('deploy', function() {
  gulp.src(config.rsync.src)
    .pipe(rsync(config.rsync.options));
});
