(function($) {
  $(function() {
  'use strict';
    var $element = $('[data-tabs-accordian]');
    if (!$element.length) {
      return;
    }

    //console.log("data-tabs-accordian");

    var $tabsName = ".c-tabs";
    var $tabsItem = $($tabsName);
    var $tabsLinkClass = $(".c-tabs__link");

    $tabsItem.each(function(index) {
      $(this).children('li').first().children('a').addClass('is-active').next().addClass('is-open').show();
    });
    $tabsItem.on('click', 'li > a.c-tabs__link', function(event) {
      if (!$(this).hasClass('is-active')) {
        event.preventDefault();
        var accordionTabs = $(this).closest($tabsName);
        accordionTabs.find('.is-open').removeClass('is-open').hide();

        $(this).next().toggleClass('is-open').toggle();
        accordionTabs.find('.is-active').removeClass('is-active');
        $(this).addClass('is-active');
      } else {
        event.preventDefault();
      }
    });
  });
})(jQuery);
