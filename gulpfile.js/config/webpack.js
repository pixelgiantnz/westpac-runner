var path            = require('path')
var paths           = require('./')
var webpack         = require('webpack')

module.exports = function(env) {

  var jsSrc = path.resolve(paths.sourceAssets + '/scripts/')
  var jsDest = paths.publicAssets + '/js/'
  var publicPath = 'assets/js/'

  var webpackConfig = {
    context: jsSrc,

    plugins: [
      new webpack.ProvidePlugin({
        "$"             : "jquery",
        "jQuery"        : "jquery",
        "window.jQuery" : "jquery"
      })
    ],

    resolve: {
      extensions: ['', '.js'],
      modulesDirectories: ['node_modules', 'bower_components', jsSrc],
      alias: {
        "vendor/jquery.slick"          : "slick-carousel/slick/slick.js",
        "vendor/jquery.fitvids"        : "fitvids/jquery.fitvids.js",
        "vendor/jquery.sticky"         : "jquery-sticky/jquery.sticky.js",
        "vendor/jquery.fitvids"        : "fitvids/jquery.fitvids.js",
        "vendor/jquery.validation"     : "jquery-validation/dist/jquery.validate.js",
        "vendor/jquery.magnific-popup" : "magnific-popup/dist/jquery.magnific-popup.js",
        "vendor/jquery.matchheight"    : "matchHeight/jquery.matchHeight.js",
        "vendor/jquery.tooltipster"    : "tooltipster/js/jquery.tooltipster.js",

        "vendor/imagesloaded"          : "imagesloaded/imagesloaded.pkgd.js",
        "vendor/slideout"              : "slideout.js/dist/slideout.js",


        "system/app"                   : "system/app.js",
        "system/loader"                : "system/loader.js",
        "system/abstract"              : "system/abstract.js",
      }
    },

    module: {
      loaders: [
        {
          test: /\.js$/,
          loader: 'babel-loader?stage=1',
          exclude: /node_modules/
        }
      ]
    }
  }

  if(env !== 'test') {
    // Karma doesn't need entry points or output settings
    webpackConfig.entry = {
      main  : ['./main.js']
    }

    webpackConfig.output = {
      path       : jsDest,
      filename   : '[name].js',
      publicPath : publicPath
    }

    // Factor out common dependencies into a common.js
    webpackConfig.plugins.push(
      new webpack.optimize.CommonsChunkPlugin({
        name     : 'common',
        filename : '[name].js',
      })
    )
  }

  if(env === 'development') {
    webpackConfig.devtool = 'source-map'
    webpack.debug = true
  }

  if(env === 'production') {
    webpackConfig.plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin(),
      new webpack.NoErrorsPlugin()
    )
  }

  return webpackConfig
}
