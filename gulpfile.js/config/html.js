var config = require('./')

module.exports = {
  watch: config.sourceDirectory + '/html/**/*.html',
  src: [config.sourceDirectory + '/html/**/*.html', '!**/{layouts,macros,parts,components,modules}/**'],
  dest: config.publicDirectory,
  nunjucks: [config.sourceDirectory + '/html/'],
  htmlmin: {
    collapseWhitespace: true
  }
}
