//the require library is configuring paths
require.config({
  //waitSeconds: 200,
  paths: {
    "jquery"     : "assets/js/libs/jquery",
    "modernizr" : "assets/js/libs/modernizr",
    "scripts" : "assets/js/scripts",

    "quintus-2d" : "assets/js/libs/quintus_2d",
    "quintus-anim" : "assets/js/libs/quintus_anim",
    "quintus-audio" : "assets/js/libs/quintus_audio",
    "quintus-input" : "assets/js/libs/quintus_input",
    "quintus-scenes" : "assets/js/libs/quintus_scenes",
    "quintus-sprites" : "assets/js/libs/quintus_sprites",
    "quintus-tmx" : "assets/js/libs/quintus_tmx",
    "quintus-touch" : "assets/js/libs/quintus_touch",
    "quintus-ui" : "assets/js/libs/quintus_ui",
    "quintus" : "assets/js/libs/quintus",
    "hammerjs" : "assets/js/libs/hammer",

    "validate"   : "assets/js/libs/jquery.validate",
    "debounced":  "assets/js/libs/debounced",
    "fitvids"    : "assets/js/libs/jquery.fitvids",
    "picturefill": "assets/js/libs/picturefill",
    "isotope": "assets/js/libs/isotope.pkgd.min",
    "isotope.packery": "assets/js/libs/packery-mode.pkgd.min",
    "featherlight":  "assets/js/libs/featherlight.min",//simple modal plugin
    "featherlight-gallery":  "assets/js/libs/featherlight.gallery",//modal gallery plugin
    "matchHeight":  "assets/js/libs/jquery.matchHeight-min",
    "slick-carousel":  "assets/js/libs/slick.min"
  },
  shim: {
    "scripts"   : {
      "deps": ["jquery"]
    }
  }
});

// load jQuery, components and blocks first - these libs are mandatory
//, 'owl', 'elastislide', 'components', 'blocks'
require(['jquery', 'modernizr', 'scripts','quintus'], function($){
});
