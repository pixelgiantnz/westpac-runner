require(['jquery','quintus-sprites','quintus-scenes','quintus-input','quintus-anim','quintus-2d','quintus-touch','quintus-ui'], function($) {

  //QUINTUS RUNNER

  var jump = false;
  var start = false;
  //var pause = false;

  //window.addEventListener("load",function() {

  var $element = $('[data-runner]');
  if (!$element.length) {
    console.log('not a runner game');
    return;
  }

 /* var hammerLayer = document.getElementById('hammerLayer');
  var hammertime = new Hammer(hammerLayer);

  Hammer.Tap({interval:0})

  hammertime.on("tap", function(ev) {
    //myElement.textContent = ev.type +" gesture detected.";
    jump = true;

  });*/



  function setInitVars()
  {
    $('.m-runner__var-input[name="topspeed"]').val(Q.state.get("playerTopSpeed"));
    $('.m-runner__var-input[name="speedmultiplier"]').val(Q.state.get("speedMultiplier"));
    $('.m-runner__var-input[name="timepenalty"]').val(Q.state.get("timePenalty"));
    $('.m-runner__var-input[name="playerjump"]').val(Q.state.get("playerJump"));
    $('.m-runner__var-input[name="firsthurdle"]').val(Q.state.get("firstHurdle"));
    $('.m-runner__var-input[name="numhurdles"]').val(Q.state.get("numHurdles"));
    $('.m-runner__var-input[name="ball2start"]').val(Q.state.get("ball2start"));
    $('.m-runner__var-input[name="conestart"]').val(Q.state.get("coneStart"));
  }

  function setVars()
  {
    //$('.m-runner__var-input[name="topspeed"]').val(Q.state.get("playerTopSpeed"));
    if($('.m-runner__var-input[name="topspeed"]').val() > 0){
      Q.state.set("playerTopSpeed",Number($('.m-runner__var-input[name="topspeed"]').val()));
    }
    if($('.m-runner__var-input[name="speedmultiplier"]').val() > 0){
      Q.state.set("speedMultiplier",Number($('.m-runner__var-input[name="speedmultiplier"]').val()));
    }
    if($('.m-runner__var-input[name="timepenalty"]').val() > 0){
      Q.state.set("timePenalty",Number($('.m-runner__var-input[name="timepenalty"]').val()));
    }
    if($('.m-runner__var-input[name="playerjump"]').val() > 0){
      Q.state.set("playerJump",Number($('.m-runner__var-input[name="playerjump"]').val()));
    }
    if($('.m-runner__var-input[name="firsthurdle"]').val() > 0){
      Q.state.set("firstHurdle",Number($('.m-runner__var-input[name="firsthurdle"]').val()));
    }
    if($('.m-runner__var-input[name="numhurdles"]').val() > 0){
      Q.state.set("numHurdles",Number($('.m-runner__var-input[name="numhurdles"]').val()));
    }
    if($('.m-runner__var-input[name="ball2start"]').val() > 0){
      Q.state.set("ball2start",Number($('.m-runner__var-input[name="ball2start"]').val()));
    }
    if($('.m-runner__var-input[name="conestart"]').val() > 0){
      Q.state.set("coneStart",Number($('.m-runner__var-input[name="conestart"]').val()));
    }


  }

  $('.js-edit-vars-btn').on('click',function()
    {
      setVars();
      $('.js-variable-edit').hide();
      $('.js-intro-message').show();
    }
  );

  //listen for keydown to enter game
  $(window).on('keydown',function(e)
    {
      //if UP key
      if(e.keyCode === 38)
      {
        jump = true;
      };

    }
  );

  $('.js-end-restart').on('click',function()
    {
      Q.clearStages();
      Q.stageScene('level1');

      Q.state.set("time",0);
      $('.js-touch-layer').show();
      $('.js-end-message').hide();
    }
  );

  $('.js-play-btn').on('click',function(){

    Q.clearStages();
    Q.stageScene('level1');

    //Q.stage().unpause();
    $('.js-touch-layer').show();
    $('.js-intro-message').hide();
    $('.js-game-display').show();
    start = true;

  });

  $('.js-touch-layer').on('touchstart',function(){
    jump = true;
  });

  // QUINTUS GAME

  var Q = window.Q = Quintus(
    {
      imagePath: "assets/images/",
      audioPath: "assets/",
      dataPath: "assets/data/"
    }
  )
  .include("Sprites, Scenes, Input, 2D, Anim, UI")
  .setup("runnerGame",
    {
      maximize: true
    }
  )

  Q.state.reset(
    {
      time: 0,
      playerTopSpeed:600,
      speedMultiplier:2,
      timePenalty:2,
      playerJump:600,
      firstHurdle:100,
      numHurdles:10,
      hurdlesGap:2000,
      ball2start:2,
      coneStart:3
    }
  );

  setInitVars();

  var SPRITE_BOX = 1;

  /*Q.input.touchControls({
    controls:  [
                 [],
                 [],
                 []]
  });*/

  /*Q.input.touchControls({
    controls:  [ ['left','<' ],
                 ['right','>' ],
                 [],
                 ['action','b'],
                 ['fire', 'a' ]]
  });*/

  Q.gravityY = 2000;
  //Q.ticker = 0;

  /*Q.gameLoop(function(dt) {
    Q.ticker++;
    console.log(Q.ticker);
    console.log("loop");
  });*/

  //console.log(Q.state);

  //Q.stage.viewport.centerOn(40 + 150, 500 );

  Q.Sprite.extend("Player",{

    init: function(p) {

      this._super(p,{
        sheet: "player",
        sprite: "player",
        /*sheet: "runner",
        sprite: "runner",*/
        collisionMask: SPRITE_BOX,
        x: 40,
        y: 555,
        standingPoints: [ [ -16, 44], [ -23, 35 ], [-23,-48], [23,-48], [23, 35 ], [ 16, 44 ]],
        duckingPoints : [ [ -16, 44], [ -23, 35 ], [-23,-10], [23,-10], [23, 35 ], [ 16, 44 ]],
        speed: 0,
        jump: Q.state.get("playerJump")*-1,
        paused:false,
        hit:false,
        hurdlesGap:Q.state.get("hurdlesGap"),
        curHurdle:0,
        numOfHurdles:Q.state.get("numHurdles"),
        ticker:0,
        topSpeed:Q.state.get("playerTopSpeed"),
        speedMultiplier:Q.state.get("speedMultiplier"),
        timePenalty:Q.state.get("timePenalty"),
        penalty:false,
        firstHurdle:Q.state.get("firstHurdle"),
        betweenObstacles:200,
        //speedMultiplier:5,
        ball:{
          startingBall:1000,
          ballGapRangeHigh:800,
          ballGapRangeLow:400
        },
        ball2:{
          startingBall:1100,
          ballGapRangeHigh:1700,
          ballGapRangeLow:1300,
          sectorStart:Q.state.get("ball2start")
        },
        cone:{
          startingCone:1300,
          coneGapRangeHigh:1200,
          coneGapRangeLow:900,
          sectorStart:Q.state.get("coneStart")
        },
        end:false


      });

      this.p.points = this.p.standingPoints;
      //this.p.curGap = this.p.hurdlesGap;
      this.p.curGap = this.p.firstHurdle;
      //this.p.curBallGap = this.p.ballGap+this.p.curGap;
      this.p.ball.curBallGap = this.p.curGap+this.p.ball.startingBall;
      this.p.ball2.curBallGap = this.p.ball2.startingBall;
      this.p.cone.curConeGap = this.p.cone.startingCone;

      this.p.obpos = {
        ballpos1:0,
        ballpos2:0,
        ballpos3:0
      };

      this.add("2d, animation");

      //Q.input.on("P",this,"pause");

      this.on("hit");

      console.log("Player top speed > "+this.p.topSpeed);
      console.log("Player speed multi > "+this.p.speedMultiplier);



    },

    step: function(dt) {

      if(!this.p.end){
        Q.state.inc("time",1/60);
      }
      //console.log(msToTime(Q.state.get("time")));

      //console.log(this.p.x);
      /*this.p.ticker++;
      console.log(this.p.ticker);*/
      var obb = this;

      if(this.p.x > this.p.curGap){

        this.p.obpos = {
          ballpos1:0,
          ballpos2:0,
          ballpos3:0
        };

        if(this.p.curHurdle == this.p.numOfHurdles){

          var line = Q("LineThrower");
          line.invoke("release");
          console.log("load line");

        }else{
          var hurdles = Q("BoxThrower");
          hurdles.invoke("release");
        }

        this.p.curGap = this.p.x+this.p.hurdlesGap;
        //this.p.hurdlesGap = this.p.hurdlesGap*0.95;
        console.log(this.p.hurdlesGap);

      }

      if(this.p.curHurdle < this.p.numOfHurdles){

        if(this.p.x > this.p.ball.curBallGap){
        //if(this.p.x > this.p.curGap){


          var balls = Q("BallThrower");
          //var balls = Q("ConeThrower");

          balls.invoke("release");

          if(this.p.curHurdle < this.p.ball2.sectorStart){
            //this.p.obpos.ballpos1 = this.obstaclePos();

            //var ballPos = this.obstaclePos();

            //console.log(ballPos);

            var ballPos = Math.random() * ((this.p.hurdlesGap-400) - (this.p.ball.ballGapRangeLow)) + this.p.ball.ballGapRangeLow;

          }else{

            var ballPos = Math.random() * (this.p.ball.ballGapRangeHigh - this.p.ball.ballGapRangeLow) + this.p.ball.ballGapRangeLow;

         }

          this.p.ball.curBallGap = this.p.curGap+ballPos;

        }

       if(this.p.x > this.p.ball2.curBallGap){

          if(this.p.curHurdle > this.p.ball2.sectorStart){
            var balls = Q("BallThrower");
            balls.invoke("release");
          }


            var ballPos2 = Math.random() * (this.p.ball2.ballGapRangeHigh - this.p.ball2.ballGapRangeLow) + this.p.ball2.ballGapRangeLow;
            //this.p.obpos.ballpos2 = this.obstaclePos();

            this.p.ball2.curBallGap = this.p.curGap+ballPos2;

        }

        //console.log(this.p.cone.curConeGap);

        if(this.p.x > this.p.cone.curConeGap){
          //console.log("release cone");
          if(this.p.curHurdle > this.p.cone.sectorStart){
            var cones = Q("ConeThrower");
            cones.invoke("release");
          }

            var conePos = Math.random() * (this.p.cone.coneGapRangeHigh - this.p.cone.coneGapRangeLow) +  this.p.cone.coneGapRangeLow;
            //this.p.obpos.ballpos3 = this.obstaclePos();
            this.p.cone.curConeGap = this.p.curGap+conePos;


        }
      }

      if(this.p.speed < this.p.topSpeed){
        if(!this.p.hit){
          //console.log(this.p.speedMultiplier +" x1000 :> "+this.p.speedMultiplier*1000);
          this.p.speed += this.p.speedMultiplier;
          //this.p.speed += 2;
        }
      }

      if(this.p.y > 555) {
        this.p.y = 555;
        this.p.landed = 1;
        this.p.vy = 0;
      } else {
        this.p.landed = 0;
        jump = false;
        //this.p.hit = false;
      }

      if(/*Q.inputs['up'] */jump && this.p.landed > 0 ) {
        this.p.vy = this.p.jump;
      }

      if(this.p.hit){
        //this.p.vy = this.p.jump;
        //this.p.vx = -10;
       //this.p.hit = false;
        this.p.y = 555;
        this.p.vx = 0;

        this.play("stop");

        if(this.p.penalty)
        {
          this.p.penalty = false;
          setTimeout(function()
            {
              obb.p.hit = false;
              console.log("timeout ended");
            },
            obb.p.timePenalty*1000
          );
        }

      }else{
        if(start){
          this.p.vx += ((this.p.speed) - this.p.vx)/4;
        }
      }

      this.p.points = this.p.standingPoints;
      if(start){
        if(this.p.landed) {

          if(!this.p.end){
            if(this.p.speed < 200){
              this.play("walk_right_slow");
            }else if(this.p.speed >= 200 && this.p.speed < 400){
              this.play("walk_right");
            }else{
              this.play("walk_right_fast");
            }
          }else{
            this.play("celebrate");
          }

        } else {
          if(this.p.hit){

          }else{
            this.play("jump_right");
          }
        }
      }else{
        this.play("stationary");
      }

      this.stage.viewport.centerOn(this.p.x + 150, 500 );

    },

    /*obstaclePos:function()
    {

      var pendingPos = Math.random() * ((this.p.hurdlesGap-this.p.betweenObstacles) - (this.p.betweenObstacles)) + this.p.betweenObstacles;
      var safePos = true;

      $.each(this.p.obpos,function(index,value){

        if(pendingPos < value+this.p.betweenObstacles && pendingPos > value-this.p.betweenObstacles){
          pendingPos = false;
        }

        //console.log(value + " | " + index);

      });

      //this.p.betweenObstacles

      return Math.random() * ((this.p.hurdlesGap-this.p.betweenObstacles) - (this.p.betweenObstacles)) + this.p.betweenObstacles;
    },*/

    hit: function()
    {
      if(!this.p.end)
      {
        //console.log("hit player");
        if(!this.p.hit)
        {
          this.p.hit = true;
          this.p.penalty = true;
          this.p.speed  = 0;
        }
      }
      //this.p.vy = this.p.jump;
    }
  });

  Q.Sprite.extend("Box",{
    init: function() {

      var levels = [ 565, 540, 500, 450 ];

      var player = Q("Player").first();

      this._super({
        x: player.p.x + Q.width + player.p.firstHurdle,
        //y: levels[Math.floor(Math.random() * 3)],
        y: 580,
        frame: 0,
        //frame: 0,
        scale: 1,
        type: SPRITE_BOX,
        sheet: "hurdles",
        //vx: -600 + 200 * Math.random(),
        vx: 0,
        vy: 0,
        ay: 0,
        maxfall:640,
        //theta: (300 * Math.random() + 200) * (Math.random() < 0.5 ? 1 : -1)
        theta: 0,
        sensor: true
      });

      this.on("sensor");
    },

    step: function(dt) {
      this.p.x += this.p.vx * dt;

      this.p.vy += this.p.ay * dt;

      //console.log(this.p.y);

      if(this.p.y < this.p.maxfall){
        this.p.y += this.p.vy * dt;
        this.p.angle += this.p.theta * dt;
      }

      /*if(this.p.y != 580) {
        this.p.angle += this.p.theta * dt;
      }*/



      //if(this.p.y > 800) { this.destroy(); }

    },

    sensor: function(colObj) {

      if(colObj.isA("Player")){

        //colObj.p.vx
        this.p.type = 0;
        this.p.collisionMask = Q.SPRITE_NONE;
        //this.p.y = 640;
        //this.p.angle = 90;
        //this.p.vx = 200;
        this.p.ay = 100;
        //this.p.x +=20;
        //this.p.vy = 300;
        //this.p.opacity = 0.5;
        this.p.theta = 110;

      }


    }


  });

  Q.GameObject.extend("BoxThrower",{
    init: function() {

      this.p = {
        launchDelay:0.75,
        launchRandom: 1,
        launch: 2

      }
    },

    update: function(dt) {

    },

    release:function(){

      var player = Q("Player").first();
      player.p.curHurdle ++;

      this.stage.insert(new Q.Box());
      console.log(player.p.curHurdle);
    }

  });

  Q.Sprite.extend("Ball",{
    init: function() {

      var player = Q("Player").first();

      this._super({
        x: player.p.x + Q.width,
        y: 605,
        frame: 0,
        scale: 1,
        type: SPRITE_BOX,
        sheet: "ball",
        //vx: -600 + 200 * Math.random(),
        vx: 0,
        vy: 0,
        ay: 0,
        sensor: true,
        theta: 0,
        maxfall:625

      });

      this.on("sensor");
    },

    step: function(dt) {
      this.p.x += this.p.vx * dt;

      this.p.vy += this.p.ay * dt;

      //console.log(this.p.y);

      if(this.p.y < this.p.maxfall){
        this.p.y += this.p.vy * dt;
        this.p.angle += this.p.theta * dt;
      }
    },

    sensor: function(colObj) {

       if(colObj.isA("Player")){

        //colObj.p.vx
        this.p.type = 0;
        this.p.collisionMask = Q.SPRITE_NONE;
        //this.p.y = 640;
        //this.p.angle = 90;
        //this.p.vx = 200;
        this.p.ay = 100;
        //this.p.x +=20;
        //this.p.vy = 300;
        //this.p.opacity = 0.5;
        this.p.theta = 310;


        //console.log("Player hit cone");
      }

    }

  });

  Q.GameObject.extend("BallThrower",{
    init: function() {
      this.p = {
        launchDelay:0.75,
        launchRandom: 1,
        launch: 2

      }
    },

    update: function(dt) {

    },

    release:function(){
      this.stage.insert(new Q.Ball());
    }

  });

  Q.Sprite.extend("Cone",{
    init: function() {

      var player = Q("Player").first();

      this._super({
        x: player.p.x + Q.width + 50,
        y: 585,
        frame: 0,
        scale: 1,
        type: SPRITE_BOX,
        sheet: "cone",
        //vx: -600 + 200 * Math.random(),
        vx: 0,
        vy: 0,
        ay: 0,
        sensor: true,
        theta: 0,
        maxfall:625

      });

      this.on("sensor");
    },

    step: function(dt) {
      this.p.x += this.p.vx * dt;

      this.p.vy += this.p.ay * dt;

      //console.log(this.p.y);

      if(this.p.y < this.p.maxfall){
        this.p.y += this.p.vy * dt;
        this.p.angle += this.p.theta * dt;
      }
    },

    sensor: function(colObj) {
      if(colObj.isA("Player")){

        //colObj.p.vx
        this.p.type = 0;
        this.p.collisionMask = Q.SPRITE_NONE;
        //this.p.y = 640;
        //this.p.angle = 90;
        //this.p.vx = 200;
        this.p.ay = 100;
        //this.p.x +=20;
        //this.p.vy = 300;
        //this.p.opacity = 0.5;
        this.p.theta = 310;


        //console.log("Player hit cone");
      }
      //console.log("hit cone");
    }

  });

  Q.GameObject.extend("ConeThrower",{
    init: function() {
      this.p = {
        launchDelay:0.75,
        launchRandom: 1,
        launch: 2

      }
    },

    update: function(dt) {

    },

    release:function(){
      //console.log("add Cone");
      this.stage.insert(new Q.Cone());
    }

  });

  Q.Sprite.extend("Line", {

    init: function(p) {

      var player = Q("Player").first();

      this._super(p,{

        sensor: true,
        vx: 0,
        vy: 0,
        x: player.p.x + Q.width + 50,
        y:600,
        gravity: 0,
        sheet: "line",
        sprite: "line",
        end:false

      });

      this.on("sensor");

    },


    sensor: function(colObj) {

      if(colObj.isA("Player")){

        //console.log("lift");
        //alert("END");
        if(!this.p.end){
        this.p.end = true;
        colObj.p.end = true;
        //alert('end');
          setTimeout(function(){
            Q.stage().pause();
            //alert("END GAME YOUR TIME WAS : "+msToTime(Q.state.get("time")));
            $('.js-end-time').html(msToTime(Q.state.get("time")));
            $('.js-touch-layer').hide();
            $('.js-end-message').show();
          },1000);
        }


      }

    }

  });

  Q.GameObject.extend("LineThrower",{
    init: function() {
      this.p = {
        launchDelay:0.75,
        launchRandom: 1,
        launch: 2

      }
    },

    update: function(dt) {

    },

    release:function(){
      //console.log("add Cone");
      this.stage.insert(new Q.Line());
    }

  });

  Q.UI.Text.extend("Time",{
    init: function(p) {
      this._super({

      });

      Q.state.on("change.time",this,"time");


    },

    time: function(time) {
      var player = Q("Player").first();
      if(start){
        this.p.label = msToTime(time);
        //console.log(this.p.label);
        $('.js-time').html(this.p.label);
      }else{
        //player.play('stationary');
        Q.stage().pause();
        this.p.label = msToTime(time);
        //console.log(this.p.label);
        $('.js-time').html(this.p.label);

      }
    }
  });


  Q.scene("prestart",function(stage) {
    stage.insert(new Q.Repeater({ asset: "background-sky.png",
                                  speedX: 0.05,
                                  /*repeatY: false,*/
                                  y:50
                                }));
    stage.insert(new Q.Repeater({ asset: "background-mountains.png",
                                  speedX: 0.1,
                                  repeatY: false,
                                  y: 200
                                }));
    stage.insert(new Q.Repeater({ asset: "background-wall.png",
                                  speedX: 0.3,
                                  repeatY: false,
                                  y: 218
                                }));

    stage.insert(new Q.Repeater({ asset: "background-floor.png",
                                  repeatY: false,
                                  speedX: 0,
                                  y: 300 }));

    stage.insert(new Q.Player());

    stage.add("viewport");

  });


  Q.scene("level1",function(stage) {
    stage.insert(new Q.Repeater({ asset: "background-sky.png",
                                  speedX: 0.05,
                                  /*repeatY: false,*/
                                  y:50
                                }));
    stage.insert(new Q.Repeater({ asset: "background-mountains.png",
                                  speedX: 0.1,
                                  repeatY: false,
                                  y: 200
                                }));
    stage.insert(new Q.Repeater({ asset: "background-wall.png",
                                  speedX: 0.3,
                                  repeatY: false,
                                  y: 218
                                }));

    stage.insert(new Q.Repeater({ asset: "background-floor.png",
                                  repeatY: false,
                                  speedX: 0,
                                  y: 300 }));

    stage.insert(new Q.BoxThrower());
    stage.insert(new Q.BallThrower());
    stage.insert(new Q.ConeThrower());
    stage.insert(new Q.LineThrower());
    stage.insert(new Q.Player());
    stage.insert(new Q.Time());
    stage.add("viewport");

    var player = Q("Player").first();
    player.p.end = false;

    console.log(player.p.end);

  });

  Q.load("player.json, player.png, runner.json, runner.png, line.json, line.png, cone.json, cone.png, ball.json, ball.png, hurdles.json, hurdles.png, background-wall.png,background-sky.png,background-mountains.png,background-floor.png", function() {
      //Q.compileSheets("crates.png","crates.json");
      Q.compileSheets("hurdles.png","hurdles.json");
      Q.compileSheets("runner.png","runner.json");
      //Q.compileSheets("player.png","player.json");
      Q.compileSheets("runner.png","player.json");
      Q.compileSheets("line.png","line.json");
      Q.compileSheets("ball.png","ball.json");
      Q.compileSheets("cone.png","cone.json");
      //Q.compileSheets("hurdles.png","hurdles.json");
      //Q.animations("runner", {
      Q.animations("player", {
        walk_right: { frames: [0,1,2,3,4,5,6,7], rate: 1/8, flip: false, loop: true },
        stop: { frames: [0], rate: 1/1, flip: false, loop: false },
        walk_right_slow: { frames: [0,1,2,3,4,5,6,7], rate: 1/3, flip: false, loop: true },
        walk_right_fast: { frames: [0,1,2,3,4,5,6,7], rate: 1/13, flip: false, loop: true },
        jump_right: { frames: [8], rate: 1/10, flip: false },
        stand_right: { frames:[0], rate: 1/10, flip: false },
        duck_right: { frames: [0], rate: 1/10, flip: false },
        celebrate: { frames: [9,10], rate: 1/10, flip: false,loop: true },
        stationary: { frames: [0], rate: 1/8, flip: false, loop: false },

        /*walk_right: { frames: [0], rate: 1/1, flip: false, loop: false },
        stop: { frames: [0], rate: 1/1, flip: false, loop: false },
        walk_right_slow: { frames: [0], rate: 1/1, flip: false, loop: false },
        walk_right_fast: { frames: [0], rate: 1/1, flip: false, loop: false },
        jump_right: { frames: [0], rate: 1/1, flip: false },
        stand_right: { frames:[0], rate: 1/1, flip: false },
        duck_right: { frames: [0], rate: 1/1, flip: false },
        celebrate: { frames: [0], rate: 1/1, flip: false,loop: false },
        stationary: { frames: [0], rate: 1/1, flip: false, loop: false },*/


      });
      //Q.stageScene("level1");
      Q.stageScene("prestart");


  });

  function msToTime(dur) {
    var duration = dur*1000;
    var milliseconds = parseInt((duration%1000)/100),
        seconds = parseInt((duration/1000)%60),
        minutes = parseInt((duration/(1000*60))%60);

    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
    //console.log(minutes + "m" + seconds + "s");
    return minutes + "m" + seconds + "s";
  }


});



