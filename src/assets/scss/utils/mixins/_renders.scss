
/// Render Properties By Media Queries
///
/// @author Eugen Figursky
/// @uses map-deep-set
/// @uses map-deep-get
///
/// @param {Map} $map - Map
/// @param {Arglist} $keys - Key chain
///
/// @example
/// margin: (
///   "#{$spacing} 0"
/// ),
/// padding-left: (
///   $spacing-s,
///   (
///     s: $spacing,
///     m: $spacing-l
///   )
/// ),
/// padding-right: (
///   $spacing,
///   (
///     s: $spacing,
///     m: $spacing-l
///   )
/// )
@mixin render-properties($properties) {

  // create empty map
  $values-by-media: ();

  // iterate through the properties
  @each $property, $propertyValues in $properties {

    // set values default value
    $values  : false;

    // grab property values from a list
    $default : nth($propertyValues, 1);

    @if (length($propertyValues) > 1) {
      $values  : nth($propertyValues, 2);
    }

    // we can render the default value
    // right away because it doesn't require any media
    @if ($default) {
      #{$property}: #{$default};
    }

    @if ($values) {
      // iterate through breakpoints and property values
      @each $breakpoint, $value in $values {
        @if ( map-has-key($values-by-media, $breakpoint) == false ) {
          $values-by-media: map-merge($values-by-media, ($breakpoint: ( $property: $value )));
        } @else {
          $values-by-media: map-deep-set($values-by-media, $breakpoint $property, $value);
        }
      }
    }
  }

  // iterate through values / breakpoints and finally render the properties.
  @each $media-breakpoint, $media-values in $values-by-media {
    $media : map-get($media-breakpoints, $media-breakpoint);
    @if $media {
      @include render-properties-values($media-values, $media);
    } @else {
      @warn "Media `media-#{$media-breakpoint}` does not exists. Please add the media or change it to existing one.";
    }
  }
}

@mixin render-property($property, $default, $values : ()) {
  @include render-properties((
    $property: ($default, $values)
  ));
}

@mixin render-properties-values($values, $media : null) {
  @if ($media) {
    @include media($media) {
      @each $property, $value in $values {
        #{$property}: #{$value};
      }
    }
  } @else {
    @each $property, $value in $values {
      #{$property}: #{$value};
    }
  }
}

/// Deep set function to set a value in nested maps
/// @author Hugo Giraudel
/// @access public
/// @param {Map} $map - Map
/// @param {List} $keys -  Key chaine
/// @param {*} $value - Value to assign
/// @return {Map}
@function map-deep-set($map, $keys, $value) {
  $maps: ($map,);
  $result: null;

  // If the last key is a map already
  // Warn the user we will be overriding it with $value
  @if type-of(nth($keys, -1)) == "map" {
    @warn "The last key you specified is a map; it will be overrided with `#{$value}`.";
  }

  // If $keys is a single key
  // Just merge and return
  @if length($keys) == 1 {
    @return map-merge($map, ($keys: $value));
  }

  // Loop from the first to the second to last key from $keys
  // Store the associated map to this key in the $maps list
  // If the key doesn't exist, throw an error
  @for $i from 1 through length($keys) - 1 {
    $current-key: nth($keys, $i);
    $current-map: nth($maps, -1);
    $current-get: map-get($current-map, $current-key);
    @if $current-get == null {
      @warn "Key `#{$current-key}` doesn't exist at current level in map.";
    }
    $maps: append($maps, $current-get);
  }

  // Loop from the last map to the first one
  // Merge it with the previous one
  @for $i from length($maps) through 1 {
    $current-map: nth($maps, $i);
    $current-key: nth($keys, $i);
    $current-val: if($i == length($maps), $value, $result);
    $result: map-merge($current-map, ($current-key: $current-val));
  }

  // Return result
  @return $result;
}

/// Map deep get
/// @author Hugo Giraudel
/// @access public
/// @param {Map} $map - Map
/// @param {Arglist} $keys - Key chain
/// @return {*} - Desired value
@function map-deep-get($map, $keys...) {
    @each $key in $keys {
        $map: map-get($map, $key);
    }
    @return $map;
}



/// Render the attributes
@mixin render-attributes($attributes) {
  @each $variation, $declarations in $attributes {
    @if ($variation == default) {
      @include render-declarations($declarations);
    } @elseif ($variation == __) {
      @include render-elements($declarations);
    } @else {
      &--#{$variation} {
        @include render-declarations($declarations);
      }
    }
  }
}

/// Render attribute declarations
@mixin render-declarations($declarations) {
  @each $property, $value in $declarations {
    $is-helper  : str-index("#{$property}", '-') == 1;
    @if ($is-helper) {
      @include render-helpers($property, $value);
    } @elseif ($property == extends) {
      @include render-extends($property, $value);
    } @else {
      #{$property}: #{$value};
    }
  }
}

/// Render helpers
@mixin render-helpers($helper, $declarations) {
  @if $helper == -pseudo {
    @include render-pseudo-declarations($declarations);
  }

  @if $helper == -media {
    @include render-media-declarations($declarations);
  }

  @if $helper == -state {
    @include render-state-declarations($declarations);
  }
}

/// Render pseudo classes
@mixin render-pseudo-declarations($pseudo-declarations) {
  @each $pseudo, $declarations in $pseudo-declarations {
    @if $pseudo == active {
      @include active {
        @include render-declarations($declarations);
      }
    } @elseif $pseudo == hover {
      @include hover {
        @include render-declarations($declarations);
      }
    } @elseif $pseudo == focus {
      @include focus {
        @include render-declarations($declarations);
      }
    } @else {
      @if ($pseudo == before-after) {
        &:before,
        &:after {
          @include render-declarations($declarations);
        }
      } @else {
        &:#{$pseudo} {
          @include render-declarations($declarations);
        }
      }
    }
  }
}

/// Render media
@mixin render-media-declarations($media-declarations) {
  @each $media, $declarations in $media-declarations {

    @if variable-exists(media-#{$media}) {
      @include media(map-get($media-breakpoints, $media)) {
        @include render-declarations($declarations);
      }
    } @else {
      @warn "Media `media-#{$media}` does not exists. Please add the media or change it to existing one.";
    }
  }
}

/// Render states attributes
@mixin render-state-declarations($state-declarations) {
  @each $state, $declarations in $state-declarations {
    // add ability to add more than one selector to the state declaration
    // default state would be .is-:state
    // Example: is-disabled, is-active
    $selector : map-get($declarations, -selector);
    @if ($selector) {
      $selector : str-replace($selector, ':state', $state);
    } @else {
      $selector : "&.is-#{$state}";
    }

    #{$selector} {
      @include render-declarations($declarations);
    }
  }
}


@mixin render-elements($elements) {
  @each $element, $declarations in $elements {
    &__#{$element} {
      @include render-attributes($declarations);
    }
  }
}


@mixin render-extends($property, $extends) {
  @extend %#{$extends};
}


