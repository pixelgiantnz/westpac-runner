var config = require('./')
var fontConfig = require('./fonts')

module.exports = {
  name           : 'Icons',
  src            : config.sourceAssets + '/icons/*.svg',
  dest           : fontConfig.dest,
  sassDest       : config.sourceAssets + '/scss/generated',
  template       : './gulpfile.js/tasks/iconFont/template.scss',
  sassOutputName : '_icons.scss',
  fontPath       : '../fonts',
  className      : 'icon',
  options        : {
    svg           : true,
    timestamp     : 0, // see https://github.com/fontello/svg2ttf/issues/33
    fontName      : 'icons',
    appendUnicode : true,
    normalize     : false
  }
}
