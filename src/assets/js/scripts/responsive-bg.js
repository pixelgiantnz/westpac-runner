(function($) {
  $(function() {
    var $element = $('[data-responsive-bg]');
    if (!$element.length) {return;}

    console.info("responsive images loaded");
    var $imageBlock = $element;

    require(['jquery', "debounced"], function() {

      var $windowWidth = $(window).width();
      testBg($windowWidth);

      $(window).smartresize(function(){
          $windowWidth = $(window).width();
          //console.log($windowWidth);
          testBg($windowWidth);
      });

      function testBg($windowWidth){
        if($windowWidth < 768 ){
          responsiveImage("data-bg-sm");
        } else if($windowWidth > 767 ){
          responsiveImage("data-bg-md");
        } 
      }

      function responsiveImage($attr){
        //console.log($attr);
        $imageBlock.each(function() {
          var $src  = $(this).attr($attr);
          $(this).css("background-image", "url("+$src+")");
        });
      }

    });

  });
})(jQuery);
