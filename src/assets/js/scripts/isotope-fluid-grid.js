(function($) {
  $(document).ready( function($) {
    var $element = $("#masonry-grid");
    if (!$element.length) {return false;}
    console.log("masonry grid");
    requirejs( [
      'assets/js/libs/isotope.pkgd.min.js'
    ], function( Isotope ) {
      
      var iso = new Isotope( "#masonry-grid", {
        itemSelector: '.c-grid-tile',
        percentPosition: true,
        masonry: {
          percentPosition: true,
          columnWidth: '#grid-sizer'
        }
      });

      // iso.once( 'arrangeComplete', function() {
      //   console.log( filteredItems.length );
      //   $iso.arrange({ filter: filterValue });
      //   $tileHeight = $gridSizer.height();
      //   console.log("$tileHeight: "+$tileHeight);
      //   $tiles.height($tileHeight);
      // });

      var $container = $('#masonry-grid');
      var $tiles = $('.c-grid-tile__info');
      var $gridSizer = $('#grid-sizer');
      var $tagsContainer = $('#filters-tags');
      var $tags = $tagsContainer.find('.c-tag');

      $tagsContainer.on( 'click', '.c-tag', function(e) {
        e.preventDefault();
        var filterValue = $(this).attr('data-filter');
        console.log("filterValue = "+filterValue);
        $tags.removeClass("active");
        $tagsContainer.find("#"+filterValue).addClass("active");
         filterMasonry(iso, filterValue);

      });

      $('#filters-select').on( 'change', function(e) {
         e.preventDefault();
        var filterValue = $(this).val();
        console.log("filterValue = "+filterValue);
        filterMasonry(iso, filterValue);

      });

    });

    function filterMasonry($iso, $val){
      if($val=="all"){
          filterValue = "*";
        } else {
          filterValue = '.'+$val;
        }
      
    }

  });

})(jQuery);

