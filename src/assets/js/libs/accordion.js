require(['jquery'], function($) {
  'use strict';

  var $element = $('[data-accordian]');
  if (!$element.length) {return;}

  // .udf.accordion = {

  // };$
  var $accordianItems = $(".b-accordian__item");
  var $accordianLinks = $(".b-accordian__link");
  var $accordianContent;

  $accordianLinks.on("click", function(){
    var $parent = $(this).parent();
    if(!$parent.hasClass("open")){
        $accordianItems.removeClass("open");
        $parent.addClass("open");
    } else {
        $parent.removeClass("open");
    }

  });

});
