var config = require('./')

module.exports = {
  autoprefixer : { browsers: ['last 2 version'] },
  src          : config.sourceAssets + "/scss/*.scss",
  dest         : config.publicAssets + '/css',
  watch        : config.sourceAssets + "/scss/**/*.scss",
  settings     : {
    indentedSyntax : false
  }
}
