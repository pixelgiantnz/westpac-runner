var config = require('./')

module.exports = {
  files  : ['public/**/*.html'],
  server : {
    baseDir : config.publicDirectory
  }
}
