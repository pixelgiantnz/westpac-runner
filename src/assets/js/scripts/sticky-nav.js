//Sticky Nav.
//Uses debounce scroll below to trigger the sticky class.
//Would be better to integrate scroll as a method for a generic debounce plugin:

(function($,sr){

	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce_scroll = function (func, threshold, execAsap) {
	    var timeout;

	    return function debounced () {
	        var obj = this, args = arguments;
	        function delayed () {
	            if (!execAsap)
	                func.apply(obj, args);
	            timeout = null;
	        };

	        if (timeout)
	            clearTimeout(timeout);
	        else if (execAsap)
	            func.apply(obj, args);

	        timeout = setTimeout(delayed, threshold || 50);
	    };
	}
	// smartresize 
	jQuery.fn[sr] = function(fn){  return fn ? this.bind('scroll', debounce_scroll(fn)) : this.trigger(sr); };

})(jQuery,'smartscroll');

(function($) {
	$(document).ready( function($) {
		console.log("sticky-nav loaded"+$('#nav-main') );
		var $header = $(".b-header");
		var $stickyClass = "is-stuck";
		$html = $("html");

		$(window).smartscroll(function(){
	        makeStickyHeader();
	    });

		function makeStickyHeader(){

			var $scrollTop = $(window).scrollTop();
			//console.log("scrollTop: "+$scrollTop);
			var $headerHeight = $header.height();

			if($scrollTop > $headerHeight){
				$html.addClass($stickyClass);
			} else {
				$html.removeClass($stickyClass);
			}
			
		}

	});
})(jQuery);
