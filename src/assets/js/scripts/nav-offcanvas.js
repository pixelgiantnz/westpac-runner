(function($) {
  $(function() {
  'use strict';

  $.udf.offcanvas = {

    $overlay: null,
    $element: null,
    $offcanvas: null,

    options: {
      bodyIsOpenedClass: 'c-offcanvas-is-opened',
      htmlIsOpenedClass: 'c-offcanvas-is-opened',
      wrapPushClass: 'c-offcanvas-push',
      isOpenedClass: 'is-opened',
      overlayClass: 'c-offcanvas-overlay'
    },

    open: function($element, $offcanvas) {
      $.udf.elements.body.addClass(this.options.bodyIsOpenedClass);
      $.udf.elements.html.addClass(this.options.htmlIsOpenedClass);
      this.$offcanvas.addClass(this.options.isOpenedClass);
      this.$element.addClass(this.options.isOpenedClass);

      $(document).trigger('offcanvas:open');
    },

    close: function() {
      $.udf.elements.body.removeClass(this.options.bodyIsOpenedClass);
      $.udf.elements.html.removeClass(this.options.htmlIsOpenedClass);
      this.$offcanvas.removeClass(this.options.isOpenedClass);
      this.$element.removeClass(this.options.isOpenedClass);
      $(document).trigger('offcanvas:close');
    },

    toggle: function() {
      if (this.$element.hasClass(this.options.isOpenedClass)) {
        this.close();
      } else {
        this.open();
      }
      $(document).trigger('offcanvas:toggle');
    },

    initOverlay: function() {
      var $this = this;
      if (null !== $this.$overlay) {
        return;
      }
      $this.$overlay = $('<div class="' + $this.options.overlayClass + '"></div>');
      $this.$element.after($this.$overlay);
    },

    init: function(element) {
      var $this = this;

      $this.$element   = $(element);
      $this.$offcanvas = $($this.$element.data('offcanvas'));
      if (!$this.$offcanvas.length) {
        return;
      }

      $this.initOverlay();

      $this.$element.on($.udf.events.click, function(e) {
        e.preventDefault();
        $this.toggle();
      });

      $this.$overlay.on($.udf.events.click, function(e) {
        e.preventDefault();
        $this.toggle();
      });

      // add class to the wrapper
      $.udf.elements.wrap.addClass($this.options.wrapPushClass);

      /*
      //nav accordian:
      var allPanels = $('.c-offcanvas-nav__menu > li ul.c-offcanvas-nav__dropdown').hide()
      
      $('.c-offcanvas-nav__item--has-dropdown > a').click(function(e) {
        e.preventDefault();
        var dropdown = $(this).next();
        if(!dropdown.hasClass("dropdown-open")){
          console.info("click");
          allPanels.slideUp();
          dropdown.slideDown().addClass("dropdown-open");
        } else {
          allPanels.slideUp().removeClass("dropdown-open");
        }

      });
      */
      //nav slide in
      var allPanels = $('.c-offcanvas-nav__menu > li ul.c-offcanvas-nav__dropdown');
      var nav = $(".c-offcanvas-nav");
      var offCanvas = $("#header-nav-offcanvas");
      var navOpen = "c-offcanvas-nav--open";
      var dropdownOpen = "c-offcanvas-nav__dropdown--open";
      var dropdown = false;
      var offCanvasWidth = 0;
      $('.c-offcanvas-nav__item--has-dropdown > a').click(function(e) {
        console.log("click off canvas nav");
        e.preventDefault();
        dropdown = $(this).next();
        if(!dropdown.hasClass(dropdownOpen)){
          nav.addClass(navOpen);
          dropdown.addClass(dropdownOpen);
          //console.info("click");
          //allPanels.slideUp();
          //dropdown.slideDown().addClass("dropdown-open");
          /*nav.animate({
            left: "-=400",
          }, 500, function() {
            // Animation complete.
            console.info("complete");
          });*/
        } else {
          nav.removeClass(navOpen);
          dropdown.removeClass(dropdownOpen);
        }

      });

      $('#offcanvas-nav .c-offcanvas-nav__item--back').click(function(e) {
        e.preventDefault();
        dropdown = $(this).parent();
        //dropdown.hide();
        nav.removeClass(navOpen);
        dropdown.removeClass(dropdownOpen).delay( 1000 );
      });

      $('#offcanvas-overlay').click(function(e) {
        e.preventDefault();
        console.log("click");
        $.udf.offcanvas.toggle();
      });

    }
  };

  $('[data-offcanvas]').each(function() {
    $.udf.offcanvas.init(this);
  });

  });
})(jQuery);
