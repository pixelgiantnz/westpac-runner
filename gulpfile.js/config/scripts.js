var config = require('./')

module.exports = {
  autoprefixer : { browsers: ['last 2 version'] },
  src          : config.sourceAssets + "/js/*.js",
  scriptsSrc   : config.sourceAssets + "/js/scripts/*.js",
  libsSrc      : config.sourceAssets + "/js/libs/*.js",
  dest         : config.publicAssets + '/js',
  libsDest     : config.publicAssets + '/js/libs',
  watch        : config.sourceAssets + "/js/**/*.js",
  settings     : {
    indentedSyntax : false
  }
}
