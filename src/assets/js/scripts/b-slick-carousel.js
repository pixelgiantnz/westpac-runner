(function($) {
    $(function() {

        var $element = $('[data-hero-carousel]');
        if (!$element.length) {
          return;
        }

        require(['jquery', "slick-carousel"], function() {
            console.log("data-hero-carousel");

            $element.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 30000,
                dots: true,
                infinite: true,
                adaptiveHeight: true,
                arrows: false
            });
        });

    });
})(jQuery);

//single image slider component
(function($) {
    $(function() {

        var $element = $('[data-slick-slider]');
        if (!$element.length) {
          return;
        }

        require(['jquery', "slick-carousel"], function() {
            console.log("data-slick-slider");

            $element.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 30000,
                dots: true,
                infinite: true,
                adaptiveHeight: true,
                arrows: true
            });
        });

    });
})(jQuery);

(function($) {
    $(function() {


        var $element = $('[data-slick-carousel]');
        if (!$element.length) {
          return;
        }

        require(['jquery', "slick-carousel"], function() {
            console.log("data-slick-carousel");

            $element.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                dots: true,
                infinite: true,
                adaptiveHeight: true,
                arrows: true
            });
        });

    });
})(jQuery);
