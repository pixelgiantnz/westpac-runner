var config = require('./')

module.exports = {
  rsync: {
    src: ["./build"],
    options: {
      destination      : "/var/www/udf",
      hostname         : "stagingwp.uniondigital.co.nz",
      username         : "root",
      root             : "build",
      incremental      : true,
      progress         : true,
      relative         : true,
      emptyDirectories : true,
      recursive        : true,
      clean            : true,
      exclude          : [".DS_Store"]
    }
  }
}
